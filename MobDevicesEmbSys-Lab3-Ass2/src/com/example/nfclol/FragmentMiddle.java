package com.example.nfclol;

import java.net.InterfaceAddress;
import java.util.ArrayList;

import android.app.Activity;
import android.app.Fragment;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.nfc.NfcAdapter;
import android.nfc.NfcManager;
import android.nfc.Tag;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.webkit.WebView.FindListener;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

public class FragmentMiddle extends Fragment {


	protected TextView etText;
	private Button btnTo;
	private Button btnFrom;
	
	private ReadModeListener rml;

	@Override
	public void onAttach(Activity activity) {
		rml = (ReadModeListener) activity;
		super.onAttach(activity);
	};
	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		View view = inflater.inflate(R.layout.fragment_middle, container, false);
		etText = (TextView) view.findViewById(R.id.et_text);
		btnTo = (Button)view.findViewById(R.id.btn_to);
		btnFrom = (Button)view.findViewById(R.id.btn_from);

		btnTo.setOnClickListener(new OnClickListener() {		
			@Override
			public void onClick(View v) {
				rml.setReadMode(false);
			}
		});
		
		btnFrom.setOnClickListener(new OnClickListener() {		
			@Override
			public void onClick(View v) {
				rml.setReadMode(true);
			}
		});
		
		return view;
	}

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

	

	}

	public interface ReadModeListener {
		public void setReadMode(Boolean readMode);
	}

}
