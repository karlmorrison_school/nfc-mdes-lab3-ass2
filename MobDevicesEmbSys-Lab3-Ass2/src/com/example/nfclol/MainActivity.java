package com.example.nfclol;

import java.nio.charset.Charset;

import com.example.nfclol.FragmentMiddle.ReadModeListener;

import android.nfc.NdefMessage;
import android.nfc.NdefRecord;
import android.nfc.NfcAdapter;
import android.nfc.NfcManager;
import android.nfc.Tag;
import android.nfc.tech.Ndef;
import android.os.Bundle;
import android.os.Parcelable;
import android.app.Activity;
import android.app.FragmentManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.view.Menu;

public class MainActivity extends Activity implements ReadModeListener {

	private NfcManager mNfcManager;
	private NfcAdapter mNfcAdapter;
	private PendingIntent mPendingIntent;
	private FragmentMiddle fragMiddle;
	private Boolean readMode = true;
	

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);

		fragMiddle = new FragmentMiddle();
		getFragmentManager().beginTransaction().replace(R.id.fragment_container, fragMiddle).commit();


		mNfcManager = (NfcManager) this.getSystemService(Context.NFC_SERVICE);
		mNfcAdapter = mNfcManager.getDefaultAdapter();

		if (mNfcAdapter == null) 
			return;

		
		
	}


	@Override 
	public void onNewIntent(Intent intent) {
		super.onNewIntent(intent); 
		if(readMode) {
			readFromTag(intent); 
		} else { 
			writeToTag(intent); 
		}

	}

	private void writeToTag(Intent intent) { 
		Tag tag = intent.getParcelableExtra(NfcAdapter.EXTRA_TAG); 
		Ndef ndef = Ndef.get(tag);
		if(ndef.isWritable()) { 
			byte[] mimeBytes = "text/plain".getBytes(Charset.forName("UTF-8"));
			
			
			
			byte[] dataBytes = String.valueOf(fragMiddle.etText.getText()).getBytes(Charset.forName("ISO-8859-1")); // ers�tt "my text� 
			
			
			NdefRecord record = new NdefRecord(NdefRecord.TNF_MIME_MEDIA, mimeBytes,new byte[0], dataBytes); 
			NdefMessage message = new NdefMessage(new NdefRecord[]{record}); 
			
			fragMiddle.etText.setText("");
			try { 
				ndef.connect(); 
				ndef.writeNdefMessage(message); 
				ndef.close(); 
			} catch (Exception e) { 

			}
		}
	}

	private void readFromTag(Intent intent) { 
		String msg = ""; 
		if(intent!=null && NfcAdapter.ACTION_NDEF_DISCOVERED.equals(intent.getAction())) { 
			Parcelable[] ndefMessages = intent.getParcelableArrayExtra(NfcAdapter.EXTRA_NDEF_MESSAGES); 
			for(Parcelable message : ndefMessages) {
				NdefRecord[] records = ((NdefMessage)message).getRecords(); 
				for(NdefRecord record : records){ 
					msg += new String(record.getPayload(),Charset.forName("ISO-8859-1"));
				} 
			} // Anv�nd Toast f�r att skriva ut msg
			fragMiddle.etText.setText(msg);
		}
	}

	@Override
	public void onPause() {
		if (mNfcAdapter != null) { 
			mNfcAdapter.disableForegroundDispatch(this); 
		}
		super.onPause(); 
	}

	@Override
	protected void onResume() {
		Intent intent = new Intent(this, getClass()).addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
		mPendingIntent = PendingIntent.getActivity(this, 0,intent, 0);
		if (mNfcAdapter != null) { 
			mNfcAdapter.enableForegroundDispatch(this, mPendingIntent, null, null);
		}
		super.onResume();
	}


	@Override
	public void setReadMode(Boolean readMode) {
		this.readMode = readMode;
	}
}
